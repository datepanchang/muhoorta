import React from 'react';

import MenuItem from 'material-ui/MenuItem';

export default class MainMenu extends React.Component {

  render() {
    return (
      <div>
        <MenuItem onTouchTap={this.props.handleDrawerClose}>All Services</MenuItem>
        <MenuItem onTouchTap={this.props.handleDrawerClose}>Service 1</MenuItem>
      </div>
    );
  }
}
