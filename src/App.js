import React from 'react';
import MainMenu from './menu/main-menu';
import MainHeader from './menu/main-header';

export default class App extends React.Component {

  render() {
    return (
      <div className="root-content">
        <MainHeader />
        <section className="main-center">
          <nav className="main-nav large-only">
            <MainMenu />
          </nav>
          <div className='main-content'>
            content
          </div>
        </section>
        <footer className="main-footer">
          <div>Date Panchang © 2017</div>
        </footer>
      </div>);
  }
}


