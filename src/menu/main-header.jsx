import React from 'react';
import Drawer from 'material-ui/Drawer';

import MenuItem from 'material-ui/MenuItem';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import NavigationExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import Logout from 'material-ui/svg-icons/action/settings-power';
import Help from 'material-ui/svg-icons/action/help-outline';
import Face from 'material-ui/svg-icons/action/face';
import Settings from 'material-ui/svg-icons/action/settings';

import MainMenu from './main-menu'

export default class MainHeader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {isDrawerOpen: false};
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this)
    this.handleDrawerClose = this.handleDrawerClose.bind(this)
  }

  handleDrawerToggle = () => this.setState({...this.state, isDrawerOpen: !this.state.isDrawerOpen})

  handleDrawerClose = () => this.setState({...this.state, isDrawerOpen: false})

  handleToggle = () => this.setState({
    ...this.state
  })

  render() {
    return (
      <header className="main-header">
        <IconButton className="small-only" onClick={this.handleDrawerToggle}>
          <NavigationMenu />
        </IconButton>
        <div>
          <span className="main-title">Muhoorta</span></div>
        <div
          className="main-header-right" style={{
          display   : 'flex',
          alignItems: 'center'
        }}
        >
          <Avatar />
          <IconMenu
            iconButtonElement={<IconButton><NavigationExpandMore /></IconButton>}
            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
          >
            <MenuItem primaryText="My Profile" leftIcon={<Face />} />
            <MenuItem primaryText="Settings" leftIcon={<Settings />} />
            <MenuItem primaryText="Help" leftIcon={<Help />} />
            <MenuItem primaryText="Sign out" leftIcon={<Logout />} />
          </IconMenu>
        </div>
        <div className="small-only">
          <Drawer
            docked={false}
            width={200}
            open={this.state.isDrawerOpen}
            onRequestChange={isDrawerOpen => this.setState({isDrawerOpen})}>
            <MainMenu handleDrawerClose={this.handleDrawerClose}/>
          </Drawer>
        </div>
      </header>
    )
  }
}

