import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {Provider} from 'react-redux';
import dpMuiTheme from './utilities/dpMuiTheme';

import App from './App';
import configureStore from './store/configureStore';

require('../node_modules/bootstrap/dist/css/bootstrap-reboot.css')
const injectTapEventPlugin = require('react-tap-event-plugin');
require('../style/global.css')

injectTapEventPlugin();
const appElement = document.getElementById('main-app-div');

if (module.hot) {
  module.hot.accept();
}

const muiTheme = getMuiTheme(dpMuiTheme);
const store = configureStore();

ReactDOM.render(
  <MuiThemeProvider muiTheme={muiTheme}>
    <Provider store={store}><App /></Provider>
  </MuiThemeProvider>, appElement
);