import {applyMiddleware, compose, createStore} from 'redux';
import makeRootReducer from './reducers';

const configureStore = (initialState = {}) => {
  const middleware = [];

  const store = createStore(
    makeRootReducer(),
    initialState,
    compose(
      applyMiddleware(...middleware)
    )
  )
  store.asyncReducers = {}

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const reducers = require('./reducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    });
  }
  return store;
}

export default configureStore;