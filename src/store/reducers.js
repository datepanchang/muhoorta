import {combineReducers} from 'redux';

const makeRootReducer = (asyncReducers) => combineReducers({});

export default makeRootReducer;
