const colors = {
  bg: '#f9eed3',
  bg_alt: '#f7e6c4',
  fg: '#056839',
  fg_bold: '#200040',
  fg_light: '#f7941e',
  border: 'rgba(160, 160, 160, 0.3)',
  border_bg: 'rgba(160, 160, 160, 0.075)',
  border_alt: 'rgba(160, 160, 160, 0.65)',
  accent: '#056839',
  dark: '#4e2e2e',
  white: '#FFF',
  black: '#191818'
};

export default colors;
