import colors from './colors'

const dpMuiTheme = {
  palette: {
    textColor: colors.black
  },
  appBar: {
    height: 50,
    color: colors.fg_light
  }
}

export default dpMuiTheme;
